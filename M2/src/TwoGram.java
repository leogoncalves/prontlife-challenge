import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class TwoGram {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Map<String, Integer> hashmap = new HashMap();
		int numberOfLetters = scanner.nextInt();
		scanner.nextLine();
		String string = scanner.nextLine();
		scanner.close();
		
		String[] items = new String[numberOfLetters-1];
		
		for (int i = 0; i < numberOfLetters-1; i++) {
			items[i] = string.substring(i, i+2);		
		}
		
		for (String item : items) {
			if(!hashmap.containsKey(item)) {
				hashmap.put(item, 1);
			} else {
				hashmap.put(item, hashmap.get(item)+1);
			}
		}
		
		Entry<String, Integer> popular = null;
		for(Entry<String, Integer> entry : hashmap.entrySet()) {
			if(popular == null || entry.getValue() > popular.getValue()) {
				popular = entry;
			}
		}
		
		System.out.println(popular.getKey());
		
	}
}
