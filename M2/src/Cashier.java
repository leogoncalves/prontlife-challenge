import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Cashier {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Map<Integer, Integer> hashmap = new HashMap();
		
		int x = scanner.nextInt();
		int y = scanner.nextInt();
		int z = scanner.nextInt();
		int acc = 0;
		while(scanner.hasNext()) {
			int a = scanner.nextInt();
			int b = scanner.nextInt();
			acc += b;
			hashmap.put(a, b);
		}
		
		int k = (int) Math.ceil((y-acc)/z);
		
		if(k == 1) {
			System.out.println(0);
		} else {
			System.out.println(k);
		}
		
		
	}
	
}
