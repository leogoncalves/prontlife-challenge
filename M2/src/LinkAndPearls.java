import java.util.Scanner;

public class LinkAndPearls {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		String necklace = scanner.nextLine();
		scanner.close();
		char[] items = necklace.toCharArray();
		int links = 0;
		int pearls = 0;
		
		for (int i = 0; i < necklace.length(); i++) {
			if(items[i] == '-') {
				links += 1;
			} else {
				pearls += 1;
			}
		}
		
		if (pearls == 0 || links % pearls == 0) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}

}
