import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

public class LessOrEqual {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Map<Integer, Integer> hashmap = new HashMap<Integer, Integer>();
		int n = scanner.nextInt();
		int k = scanner.nextInt();
		int[] list = new int[n];
		scanner.nextLine();
		
		for (int i = 0; i < n; i++) {
			int item = scanner.nextInt();			
			list[i] = item;
		}
		
		Arrays.sort(list);
		
		for (int i = 0; i < list.length; i++) {
			System.out.println(list[i]);
		}
		
		int acc = 0;
		for (int i = 0; i < list.length; i++) {
			if(list[i] <= k) {
				acc++;
			} else {
				break;
			}
		}
		System.out.println((acc == 0) ? -1 : acc);
		
		
	}

}
