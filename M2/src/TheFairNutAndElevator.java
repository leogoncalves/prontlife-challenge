import java.util.Scanner;

public class TheFairNutAndElevator {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int[] list = new int[n];
		int acc = 0;
		
		for (int i = 0; i < n; i++) {
			int item = scanner.nextInt();			
			list[i] = item;
			acc+= item * (i+1) * 2;
		}
		
		scanner.close();
		
		System.out.println(acc);

	}

}
