import java.util.ArrayList;
import java.util.Scanner;

public class InsideOut {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int quantidadeDeFrases = scanner.nextInt();
		scanner.nextLine();
		for (int i = 0; i < quantidadeDeFrases; i++) {
			String frase = scanner.nextLine();
			String p1 = frase.substring(0, frase.length()/2);
			String p2 = frase.substring(frase.length()/2, frase.length());
			System.out.println(new StringBuffer(p1).reverse().toString() + new StringBuffer(p2).reverse().toString());
		}
		
		
		scanner.close();
	}
}

