import java.util.Scanner;

public class DDoS {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int input = scanner.nextInt();		
		
		int count = 0;
		int maxSequence = 0;
		int temp;
		
		for (int i = 0; i < input; i++) {
			temp = scanner.nextInt();
			if(temp > 100) {
				count += 1;
				maxSequence = count;
			}
			else {
				if(count > maxSequence) {
					maxSequence = count;
				}
				count = 0;
			}
		}
		
		scanner.close();

		System.out.println(maxSequence);
		
	}
}