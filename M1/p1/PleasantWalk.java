import java.util.Scanner;

public class PleasantWalk {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		// read statements
		int numberOfHouses = scanner.nextInt();
		int numberOfColors = scanner.nextInt();
		
		int iter = 0;
		int count = 0;
		int maxSequence = 0;
		int temp;
		
		for (int i = 0; i < numberOfHouses; i++) {			
			temp = scanner.nextInt();
			if(temp != iter){				
				count += 1;
				iter = temp;
				if(count > maxSequence) {
					maxSequence = count;
				}
			}
			else {
				if(count > maxSequence) {
					maxSequence = count;
				}
				count = 1;
				iter = temp;
			}			
		}		
		
		scanner.close();
		
		System.out.println(maxSequence);
	}
}

