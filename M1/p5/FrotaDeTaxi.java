import java.util.Scanner;

public class FrotaDeTaxi {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		
		float precoPorLitroAlcool = scanner.nextFloat();
		float precoPorLitroGasolina = scanner.nextFloat();
		float rendimentoComAlcool = scanner.nextFloat();
		float rendimentoComGasolina = scanner.nextFloat();
		
		String result = null;
		
		double etanol = precoPorLitroAlcool / rendimentoComAlcool;
		double gasolina = precoPorLitroGasolina / rendimentoComGasolina;
		
		if (etanol > gasolina) result = "G";
		if (etanol < gasolina) result = "A";
		if (etanol == gasolina) result = "G";
		
		scanner.close();
		System.out.println(result);
		
	}

}

